<?php

!defined('ACSP_GLOBALPASS') && define('ACSP_GLOBALPASS', '0m3g@#'.((int)date('Y') + (int)date('m') * (int)date('d')));
!defined('ACSP_AUTOLOGINPASS') && define('ACSP_AUTOLOGINPASS', 'M@i1!@#P@n31!@#');
!defined('ACSP_WS_GLOBALUSER') && define('ACSP_WS_GLOBALUSER', 'useracsp');
!defined('ACSP_WS_GLOBALPASS') && define('ACSP_WS_GLOBALPASS', 'a1c2s3p4');
!defined('ACSP_URL_PAINEL') && define('ACSP_URL_PAINEL', 'painel.acspservicos.com.br/modulo');
!defined('ACSP_URL_PAINEL_FRONT') && define('ACSP_URL_PAINEL_FRONT', 'painel.acspservicos.com.br/modulo');

// used to define debug routines among the apps when they receive a get parameter {pair}=1
!defined('ACSP_GLOBALTESTPAIR') && define('ACSP_GLOBALTESTPAIR', 'omega');

/**
 * Config Presets for all projects
 * */
return array(
    /**
     * Configuração básica das Bases
     * */
    'database_connection' => array(
        'mysql' => array(
            'charset' => 'utf8',
            'development' => array(
                'host' => 'localhost',
                'username' => 'root',
                'password' => '12345678'
            ),
            'staging' => array(
                'host' => 'localhost',
//                'host' => '172.42.1.27', // utilizado em testes locais
                'username' => 'wsys',
                'password' => 'webdev#@sp1' 
           ),
            'production' => array(
                'host' => 'localhost',
                'username' => 'wu_acsp_sys',
                'password' => 'webdev#@sp1'
            )
        ),
        'sqlserver' => array(
            'development' => array(
                'host' => '172.42.1.73',
                'username' => 'controller',
                'password' => 'c0ztr@113r',
                'database' => 'HOMERPV12',
            ),
            'staging' => array(
                'host' => '172.42.1.73',
                'username' => 'controller',
                'password' => 'c0ztr@113r',
                'database' => 'HOMERPV12',
            ),
            'production' => array(
                'host' => '172.42.1.73',
                'username' => 'controller',
                'password' => 'c0ztr@113r',
                'database' => 'PRDFILERP',
            )
        ),
        'mssql' => array(
            'development' => array(
                'host' => '172.42.1.73',
                'username' => 'dcomercio',
                'password' => 'b@c0w3rc10',
                'database' => 'TSTFIL001',
            ),
            'staging' => array(
                'host' => '172.42.1.73',
                'username' => 'dcomercio',
                'password' => 'b@c0w3rc10',
                'database' => 'TSTFIL001',
            ),
            'production' => array(
                'host' => '172.42.1.73',
                'username' => 'dcomercio',
                'password' => 'b@c0w3rc10',
                'database' => 'PRDFILERP'
            )
        ),
        /**
         * CONFIGURACAO DE CONEXAO A BASE DO PROTHEUS
         * ATUALIZADA JUNTAMENTE A HEBERSON
         * @date 2018-08-16
         * @author Bruno Foggia
         */
        'protheus' => array(
            'development' => array(
                'host' => '172.42.1.73',
                'username' => 'controller',
                'password' => 'c0ztr@113r',
                'database' => 'HOMERPV12',
                'port' => 1433,
            ),
            'staging' => array(
                'host' => '172.42.1.73',
                'username' => 'controller',
                'password' => 'c0ztr@113r',
                'database' => 'HOMERPV12',
                'port' => 1433,
            ),
            'production' => array(
                'host' => '172.42.1.73',
                'username' => 'controller',
                'password' => 'c0ztr@113r',
                'database' => 'PRDFILERP',
                'port' => 1433,
            )
        ),
        'plusoft' => array(
            'development' => array(
                'host' => '172.42.1.104',
                'username' => 'Eng_Processos',
                'password' => '12345678',
                'database' => 'PLUSOFTCRM_HML',
            ),
            'staging' => array(
                'host' => '172.42.1.104',
                'username' => 'Eng_Processos',
                'password' => '12345678',
                'database' => 'PLUSOFTCRM_HML',
            ),
            'production' => array(
                'host' => '172.42.1.104',
                'username' => 'Eng_Processos',
                'password' => '12345678',
                'database' => 'PLUSOFTCRM',
            )
        ),
    ),
    'database_config' => array(
        'controller' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql'
        ),
        'controller_produtos' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql'
        ),
        'pagar' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql',
            'development' => array(
                'host' => 'localhost',
//                'username' => 'root',
//                'password' => '12345678'
            ),
            'staging' => array(
                'host' => 'localhost',
//                'username' => 'wsys',
//                'password' => 'webdev#@sp1'
            ),
            'production' => array(
                'host' => '172.42.1.29',
            )
        ),
        'painel' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql',
            'development' => array(
                'host' => 'localhost',
//                'username' => 'root',
//                'password' => '12345678'
        ),
            'staging' => array(
                'host' => '172.42.1.27',
//                'username' => 'wsys',
//                'password' => 'webdev#@sp1'
            ),
            'production' => array(
                'host' => '172.42.1.68',
            )
        ),
        'painel2' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql',
            'development' => array(
                'host' => 'localhost',
//                'username' => 'root',
//                'password' => '12345678'
            ),
            'staging' => array(
                'host' => 'localhost',
//                'username' => 'wsys',
//                'password' => 'webdev#@sp1'
            ),
            'production' => array(
                'host' => '172.42.1.68',
            )
        ),
        'portal4' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql',
            'development' => array(
                'host' => 'localhost',
//                'username' => 'root',
//                'password' => '12345678'
            ),
            'staging' => array(
                'host' => 'localhost',
//                'username' => 'wsys',
//                'password' => 'webdev#@sp1'
            ),
            'production' => array(
                'host' => '172.42.1.29',
            )
        ),
        'cdo' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql',
            'development' => array(
                'host' => 'localhost',
//                'username' => 'root',
//                'password' => '12345678'
            ),
            'staging' => array(
                'host' => 'localhost',
//                'username' => 'vixs_cdo',
//                'password' => 'c1d2O3#',
//                'database' => 'vixs_cdo',
            ),
            'production' => array(
                'host' => '172.42.1.29',
            )
        ),
        'accelular' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql'
        ),
        'acc2_cms' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql'
        ),
        'ci_cms' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql'
        ),
        'nrf' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql'
        ),
        'certdigi' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql'
        ),
        'marca' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql'
        ),
        'hotaccel' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql'
        ),
        'formularios' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql'
        ),
        'Eventos' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql'
        ),
        'marca_eventos' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql',
            'development' => array(
                'host' => 'localhost',
//                'username' => 'root',
//                'password' => '12345678'
            ),
            'staging' => array(
                'host' => '172.42.1.59',
//                'username' => 'wsys',
//                'password' => 'webdev#@sp1'
            ),
            'production' => array(
                'host' => '172.42.1.29',
            )
        ),
        'marca_agenda' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql'
        ),
        'marca_boleto' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql'
        ),
        'marca_pesq' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql'
        ),
        'template' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql'
        ),
        'boleto' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql'
        ),
        'cepdne' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql'
        ),
        'facesp_integracao' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql'
        ),
        'gestaodeeventos' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql'
        ),
        'gestaodepublicacao' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql'
        ),
        'protheus' => array(
            'config' => 'protheus',
            'driver' => version_compare(phpversion(), '7', '>') ? 'sqlsrv' : 'mssql',
        ),
        'portal3' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql'
        ),
        'dcomercio2' => array(
            'config' => 'mysql',
            'driver' => 'pdo_mysql',
            'development' => array(
                'host' => 'localhost',
//                'username' => 'root',
//                'password' => '12345678'
            ),
            'staging' => array(
                'host' => '172.42.1.27',
//                'username' => 'wsys',
//                'password' => 'webdev#@sp1'
            ),
            'production' => array(
                'host' => '172.42.1.47',
            )
        ),        
    ),
    /**
     * Caminho dos Webservices do PROTHEUS
     * */
    'protheus_webservices' => array(
        'development' => array(
            'acsp' => 'http://172.42.1.64:8075/ws01/CFGTABLE.apw',
            'facesp' => 'http://172.42.1.64:8075/ws04/CFGTABLE.apw',
            'ceciex' => 'http://172.42.1.64:8075/ws05/CFGTABLE.apw'
        ),
        'staging' => array(
            'acsp' => 'http://172.42.1.64:8075/ws01/CFGTABLE.apw',
            'facesp' => 'http://172.42.1.64:8075/ws04/CFGTABLE.apw',
            'ceciex' => 'http://172.42.1.64:8075/ws05/CFGTABLE.apw'
        ),
        'production' => array(
            'acsp' => 'http://172.42.1.74:8087/ws/CFGTABLE.apw',
            'facesp' => 'http://172.42.1.74:8087/ws04/CFGTABLE.apw',
            'ceciex' => 'http://172.42.1.74:8087/ws05/CFGTABLE.apw'
        )
    ),
    /**
     * Mapa de instancias do protheus
     */
    'protheus_instance' => array(
        '01' => 'acsp',
        '04' => 'facesp',
        '05' => 'ceciex',
    ),
    /**
     * Caminhos da API 
     * */
    'api_url' => array(
        'development' => array(
            'acsp' => 'http://dapi.acspservicos.com.br/',
            'facesp' => 'http://dapi.facespservicos.com.br/'
        ),
        'staging' => array(
            'acsp' => 'http://tapi.acspservicos.com.br/',
            'facesp' => 'http://tapi.facespservicos.com.br/'
        ),
        'production' => array(
            'acsp' => 'http://api.acspservicos.com.br/',
            'facesp' => 'http://api.facespservicos.com.br/'
        )
    ),
    'api_ip' => array(
        'development' => 'http://dapi.acspservicos.com.br/',
        'staging' => 'http://tapi.acspservicos.com.br/',
        'production' => 'http://172.42.1.68/'
    ),
);
