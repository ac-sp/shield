<?php

namespace acsp\shield\Models;

class Token extends \HiMax\model\Token {
    protected $database = 'painel';
    protected $table = 'PN_ACCESS_TOKEN';
    
    protected $foreignKeys = [
        'user' => [
            'type' => \doctrine\Dashes\BELONGSTO,
            'key' => 'user_id',
            'model' => '\acsp\shield\Models\User'
        ],
    ];

    protected $fieldsAliases = [
        'ID' => 'id',
        'TOKEN' => 'token',
        'USERID' => 'user_id',
    ];
    
    public function formatSystemUrl($url, $token=false) {
        $aliases = $this->getAliasesFields();
        $url = \acsp\helpers\Url::ambienteUrl($url);
        
        return $url.(!$token ? '' : '?token='.urlencode(\HiMax\Core::getMe()->getData('token')[$aliases['token']]));
    }
}
