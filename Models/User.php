<?php

namespace acsp\shield\Models;

class User extends \HiMax\model\User {
    protected $database = 'painel';
    protected $table = 'WGERUSERS';
    protected $primaryKey = 'id';
    protected $statusValue = 'Ativo';
    protected $foreignKeys = [
        'acl' => [
            'type' => \doctrine\Dashes\HASMANY,
            'key' => 'user_id',
            'model' => '\acsp\shield\Models\Acl'
        ],
        'token' => [
            'type' => \doctrine\Dashes\HASMANY,
            'key' => 'user_id',
            'model' => '\acsp\shield\Models\Token'
        ],
        'department' => [
            'type' => \doctrine\Dashes\BELONGSTO,
            'key' => 'department_id',
            'model' => '\acsp\shield\Models\Department'
        ],
    ];
    protected $fieldsAliases = [
        'ID' => 'id',
        'NOVASENHA' => 'pass',
        'LOGIN' => 'user',
        'EMAIL' => 'email',
        'CPF' => 'doc',
        'NOME' => 'name',
        'DEPTO' => 'department_id',
        'STATU' => 'status',
    ];
    
    public function login_formatUser($userData) {
        if(empty($userData)) return $userData;
        
        $result = parent::login_formatUser($userData);
        $result['department'] = !empty($userData['department']) ? [$userData['department']] : [];
        
        $result['departmentId'] = array_map(function($item) {
            return $item['id'];
        }, $result['department']);
        
        return $result;
    }
    
    /**
     * Get A list of systems the user is allowed to access
     */
    public function getSystemList() {
        $modelSystem = new System();
        $systemList = $modelSystem->find();
        
        $results = [];
        $legadoAcl = $this->getLegadoAccess(\HiMax\Core::getMe()->getData('user')['id']);
        foreach($systemList as $x => $systemItem) {
            $systemItem['url'] = $this->formatSystemUrl($systemItem['url'], true);
            if(!empty($systemItem['legado']) && !empty($legadoAcl[$systemItem['legado']])) {
                $results[$systemItem['nome']] = $systemItem;
            } else if(!empty($systemItem['rota'])) {
                @list($module, $controller, $action) = explode('/', $systemItem['rota']);
                if(\HiMax\Core::getMe()->checkAccess($controller, $action, $module, null, $systemItem['id'])===true) {
                    $results[$systemItem['nome']] = $systemItem;
                }
            }
        }
        ksort($results);
        return $results;
    }
    
    protected function getLegadoAccess($userId) {
        $model = new Pmuperm();
        return $model->getLegadoAccess($userId);
    }

}
