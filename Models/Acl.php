<?php

namespace acsp\shield\Models;

class Acl extends \HiMax\model\Acl {
    protected $database = 'painel';
    protected $table = 'PNACL';
 
    protected $foreignKeys = [
        'profile' => [
            'type' => \doctrine\Dashes\BELONGSTO,
            'key' => 'profile_id',
            'model' => '\acsp\shield\Models\Profile'
        ],
    ];
    
    protected $fieldsAliases = [
        'usuario_id' => 'user_id',
        'perfil_id' => 'profile_id',
    ];
    
}
