<?php

namespace acsp\shield\Models;

class Pmuperm {
    
    use \doctrine\Dashes\Model;
    
    protected $database = 'painel';
    protected $table = 'PMUPERM';
    protected $deactivate = false;

    protected $fieldsAliases = [
        'VALOR' => 'status',
        'USERID' => 'user_id',
        'PERMID' => 'system_id',
    ];
    
    /**
     * Get acl to old systems [its a short list, 'cause there's just one by system
     * @param integer $userId
     * @return string
     */
    public function getLegadoAccess($userId) {
        $results = $this->find([
            'user_id' => $userId,
            'status' => '1'
        ]);
        $aliases = $this->getAliasesFields();

        $acl = [];
        if (!empty($results)) {
            foreach ($results as $row) {
                $acl[$row[$aliases['system_id']]] = ['*'];
            }
        }
        return $acl;
    }
}
