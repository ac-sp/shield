<?php

namespace acsp\shield;

/**
 * Vendor
 * 
 * Provides info about vendors and info located there
 * 
 * @package     Shield
 * @category	Security
 * @author	Bruno Foggia
 * @link	https://bitbucket.org/
 */
abstract class Vendor {
    protected static $vendorData = [];
    protected static $vendorDbList = [];

    /**
     * Get Vendor dir and Current Environment
     * @return type
     */
    public static function getVendorData() {
        if(empty(static::$vendorData)) {
            $environment = require(__DIR__ . '/global-environment.php');
            static::$vendorData = ['env' => $environment];
        }
        return static::$vendorData;
    }

    /**
     * Get DB List from vendor dir
     * @return array
     */
    public static function getVendorDbList() {
        if(empty(static::$vendorDbList)) {
            $dblist = [];
            $vendorData = static::getVendorData();
            $config = require __DIR__ . '/' . "global-config.php";

            foreach ($config['database_config'] as $dbid => $dbconfig) {
                $dbconfigenv = (array) @$dbconfig[$vendorData['env']];
                $connconfig = $config['database_connection'][$dbconfig['config']][$vendorData['env']];
                if(is_array($connconfig)) {
                    $dbname = array_key_exists('database', $connconfig) ? $connconfig['database'] : 
                        (array_key_exists('database', $dbconfigenv) ? $dbconfigenv['database'] : $dbid);

                    $dblist[$dbid] = [
                        'host' => !empty($dbconfigenv['host']) ? $dbconfigenv['host'] : $connconfig['host'],
                        'user' => !empty($dbconfigenv['username']) ? $dbconfigenv['username'] : $connconfig['username'],
                        'pass' => !empty($dbconfigenv['password']) ? $dbconfigenv['password'] : $connconfig['password'],
                        'dbname' => $dbname,
                        'driver' => $dbconfig['driver'],
                    ];
                    
                    !empty($connconfig['port']) && ($dblist[$dbid]['port'] = $connconfig['port']);
                    
                    $charset = !empty($config['database_connection'][$dbconfig['config']]['charset']) ? $config['database_connection'][$dbconfig['config']]['charset'] : NULL;
                    !empty($charset) && ($dblist[$dbid]['charset'] = $charset);
                    
                    $dblist[$dbid]['host']===$_SERVER['SERVER_ADDR'] && ($dblist[$dbid]['host'] = 'localhost');
                }
            }
            static::$vendorDbList = $dblist;
        }

        return static::$vendorDbList;
    }

}
